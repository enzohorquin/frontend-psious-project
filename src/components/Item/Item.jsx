import React from 'react';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './styles.css';
export const Item = ({ id_task, description, deleteToDo }) => (
    <li key={id_task}>
        <div >
            <span className={"word"}>{description}</span>
            <FontAwesomeIcon className="close" icon={faTrashAlt} style={{ 'cursor': 'pointer' }} onClick={() => deleteToDo(id_task)} />
        </div>
    </li>
)

export default Item;