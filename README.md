Used the Redux-Saga architecture for the application.
Use bootstrap for the CSS, and use local storage to store the user token that comes from the backend.
Each component is located within the 'components' folder and within a specific folder, and within each folder there are two files an index.js (Container) and a jsx file.
I used a HOC withAuth to handle authentication of each route / component (Check if there is a token in localStorage and it is not logged in).

