import { SET_ERRORS } from '../actions';

const initialState = {
    errors: {

    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_ERRORS:
            return action.payload;
        default:
            return state;
    }
}