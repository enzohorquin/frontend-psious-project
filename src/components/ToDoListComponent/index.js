import ToDoListComponent from './ToDoListComponent';
import { connect } from 'react-redux';
import { SET_ITEMS_REQUEST, ADD_ITEM_REQUEST, DELETE_ITEM_REQUEST, CLEAR_ITEMS_REQUEST, RESTORE_LOGIN, LOGOUT_REQUESTED } from '../../actions';
import { makeActionCreator } from '../../utils/actionCreators';

const mapStateToProps = state => ({
    list: state.list.list,
    errors: state.errors.errors,
    isLogged: state.isAuthenticated

});

const mapDispatchToProps = dispatch => ({
    getToDoList: () => dispatch(makeActionCreator(SET_ITEMS_REQUEST)),
    addToDo: (payload) => dispatch(makeActionCreator(ADD_ITEM_REQUEST, payload)),
    deleteToDo: (payload) => dispatch(makeActionCreator(DELETE_ITEM_REQUEST, payload)),
    clearList: () => dispatch(makeActionCreator(CLEAR_ITEMS_REQUEST)),
    restoreLogin: payload => dispatch(makeActionCreator(RESTORE_LOGIN, payload)),
    logOut: (payload) => dispatch(makeActionCreator(LOGOUT_REQUESTED, payload)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ToDoListComponent);
