import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import Login from "../Login";
import SignUp from "../SignUp";
import ToDoListComponent from '../ToDoListComponent';

export const Main = () =>
    (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" render={() => (
                    <Redirect to="/login" />
                )} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={SignUp} />
                <Route path='/home' component={ToDoListComponent} />
            </Switch>
        </BrowserRouter>
    )
export default Main;