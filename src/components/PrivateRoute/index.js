import PrivateRoute from './PrivateRoute';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    isLoggedIn: state.isAuthenticated
});

export default connect(
    mapStateToProps,
    undefined
)(PrivateRoute);
