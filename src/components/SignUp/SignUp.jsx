import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { Link } from 'react-router-dom';
import withAuth from '../withAuth';

class SignUp extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      passwordConfirm: ""
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.errors;
  }

  _onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  _onSubmit = e => {
    const { username, password, passwordConfirm } = this.state;
    const { history } = this.props;
    e.preventDefault();
    const user = {
      username,
      password,
      passwordConfirm,
      history
    };
    this.props.signUp(user);
  };

  render() {
    const { errors } = this.props;
    return (
      <div className="container" style={{ marginTop: "50px", width: "700px" }}>
        <h2 style={{ marginBottom: "40px" }}>Registration</h2>
        <form onSubmit={this._onSubmit} onChange={this._onChange}>
          <div className="form-group">
            <input
              type="text"
              placeholder="Username"
              className={classnames("form-control form-control-lg", {
                "is-invalid": errors.username
              })}
              name="username"
            />
            {errors.username && (
              <div className="invalid-feedback">{errors.username}</div>
            )}
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Password"
              className={classnames("form-control form-control-lg", {
                "is-invalid": errors.password
              })}
              name="password"
            />
            {errors.password && (
              <div className="invalid-feedback">{errors.password}</div>
            )}
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Confirm Password"
              className={classnames("form-control form-control-lg", {
                "is-invalid": errors.password_confirm
              })}
              name="passwordConfirm"
            />
            {errors.password_confirm && (
              <div className="invalid-feedback">{errors.password_confirm}</div>
            )}
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Register User
            </button>
            <Link to="/login" className="btn btn-link">Already have an account? Log in</Link>
          </div>
        </form>
      </div>
    );
  }
}

SignUp.propTypes = {
  signUp: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default withAuth(SignUp);
