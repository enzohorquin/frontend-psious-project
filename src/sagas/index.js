import { all, fork } from 'redux-saga/effects';
import { watchLogin, watchSignUp, watchRestore, watchLogOut } from './authSagas';
import {
  watchAddTask,
  watchDeleteTask,
  watchClearTask,
  watchGetTask,
} from './toDoSagas';

export default function* rootSagas() {
  yield all([
    fork(watchLogin),
    fork(watchSignUp),
    fork(watchRestore),
    fork(watchLogOut),
    fork(watchAddTask),
    fork(watchDeleteTask),
    fork(watchClearTask),
    fork(watchGetTask)
  ]);
}
