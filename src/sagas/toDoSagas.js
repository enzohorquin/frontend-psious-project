import { call, put, takeEvery } from "redux-saga/effects";
import {
  addTask,
  deleteTask,
  getTasks,
  clearTasks
} from "../services/taskService";
import {
  ADD_ITEM_REQUEST,
  ADD_ITEM_SUCCESS,
  DELETE_ITEM_REQUEST,
  DELETE_ITEM_SUCCESS,
  CLEAR_ITEMS_REQUEST,
  CLEAR_ITEMS_SUCCESS,
  SET_ITEMS_REQUEST,
  SET_ITEMS_SUCCESS,
  SET_ERRORS,
} from "../actions";

export function* add(action) {
  try {

    const response = yield call(addTask, action.payload);
    yield put({
      type: ADD_ITEM_SUCCESS,
      payload: {
        id_task: response.data.insertId,
        description: action.payload.description
      }
    });
  } catch (e) {
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: {
          add: 'Could not add new Item'
        }
      }
    });
  }
}

export function* deleteItem(action) {
  try {
    yield call(deleteTask, action.payload);
    yield put({
      type: DELETE_ITEM_SUCCESS,
      payload: action.payload
    });
  } catch (e) {
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: {
          delete: 'Could not delete Item'
        }
      }
    });
  }
}

export function* clearTask(action) {
  try {
    yield call(clearTasks, action).payload;
    yield put({
      type: CLEAR_ITEMS_SUCCESS
    });
  } catch (e) {
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: {
          clear: 'Could not clear list'
        }
      }
    });
  }
}

export function* getTask(action) {
  try {
    const response = yield call(getTasks, action);
    yield put({
      type: SET_ITEMS_SUCCESS,
      payload: response.data
    });
  } catch (e) {
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: {
          message: 'Could not load list from server'
        }
      }
    });
  }
}

export function* watchAddTask() {
  yield takeEvery(ADD_ITEM_REQUEST, add);
}

export function* watchDeleteTask() {
  yield takeEvery(DELETE_ITEM_REQUEST, deleteItem);
}

export function* watchClearTask() {
  yield takeEvery(CLEAR_ITEMS_REQUEST, clearTask);
}

export function* watchGetTask() {
  yield takeEvery(SET_ITEMS_REQUEST, getTask);
}
