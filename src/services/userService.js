export const signUpService = request => {
  const END_POINT = 'http://localhost:3000/user/signup';
  const parameters = {
    username: request.payload.username,
    password: request.payload.password,
    passwordConfirm: request.payload.passwordConfirm
  }
  return makeApiCall(END_POINT, parameters);
};

export const logInService = request => {
  const END_POINT = 'http://localhost:3000/user/login';
  const parameters = {
    username: request.payload.username,
    password: request.payload.password
  }
  return makeApiCall(END_POINT, parameters);
};

export const makeApiCall = (endpoint, parameters) => {
  return fetch(endpoint, {
    body: JSON.stringify(parameters),
    method: 'POST',
    headers: { 'Content-Type': 'application/json' }
  })
    .then(results => results.json()).then(results => {
      if (results.errors) return Promise.reject(results.errors);
      return results;
    })
    .catch(err => {
      return Promise.reject(err);
    });
};
export default {
  signUpService,
  logInService
};
