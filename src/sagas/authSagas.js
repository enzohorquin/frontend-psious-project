import { call, put, takeLatest } from 'redux-saga/effects';
import userService from '../services/userService';

import {
  LOGIN_SUCCESS,
  LOGIN_REQUEST,
  SIGNUP_REQUEST,
  SET_ERRORS,
  SIGNUP_SUCCESS,
  RESTORE_LOGIN,
  LOGOUT_REQUESTED,
  LOGOUT_COMPLETED,
  CLEAR_ITEMS_SUCCESS,
} from '../actions';
import { setToken, getDecodedToken, removeToken } from '../utils/localStorage';

const { signUpService, logInService } = userService;

function* initiateLogin(action) {
  try {
    const { history } = action.payload;
    const response = yield call(logInService, action);
    let token;
    let user;
    token = response.data;
    yield call(setToken, token);
    user = yield call(getDecodedToken);
    yield put({
      type: LOGIN_SUCCESS,
      payload: {
        user
      }
    });
    history.push('/home');
  }
  catch (e) {

    console.log(e);
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: e
      }
    });
  }
}

export function* watchLogin() {
  yield takeLatest(LOGIN_REQUEST, initiateLogin);
}

export function* initiateSignUp(action) {

  try {
    const { history } = action.payload;
    const response = yield call(signUpService, action);
    yield put({ type: SIGNUP_SUCCESS, payload: response });
    history.push('/login');
  }
  catch (e) {

    yield put({ type: SET_ERRORS, payload: e });

  }
}

export function* watchSignUp() {
  yield takeLatest(SIGNUP_REQUEST, initiateSignUp);
}

export function* restore(action) {
  try {
    const { history } = action.payload;
    const user = yield call(getDecodedToken);
    yield put({
      type: LOGIN_SUCCESS,
      payload: {
        user
      }
    });
    history.push('/home')
  }
  catch (e) {
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: e
      }
    });
  }
}

export function* watchRestore() {
  yield takeLatest(RESTORE_LOGIN, restore);
}
export function* initiateLogOut(action) {
  try {
    const { history } = action.payload;
    removeToken();
    yield put({
      type: LOGOUT_COMPLETED,
    });
    yield put({
      type: CLEAR_ITEMS_SUCCESS
    })
    history.push('/login')
  } catch (e) {
    yield put({
      type: SET_ERRORS,
      payload: {
        errors: e
      }
    });
  }
}
export function* watchLogOut() {
  yield takeLatest(LOGOUT_REQUESTED, initiateLogOut);
}