import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducer';
import rootSaga from '../sagas';

const logger = createLogger();
const saga = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = [
    saga,
    logger,

]

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middlewares))
);

saga.run(rootSaga);

export default store;