import SignUp from "./SignUp";
import { connect } from "react-redux";
import { SIGNUP_REQUEST, RESTORE_LOGIN } from "../../actions";
import { makeActionCreator } from '../../utils/actionCreators';

const mapStateToProps = state => ({
  errors: state.errors,
  isLogged: state.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  signUp: payload => dispatch(makeActionCreator(SIGNUP_REQUEST, payload)),
  restoreLogin: payload => dispatch(makeActionCreator(RESTORE_LOGIN, payload)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);
