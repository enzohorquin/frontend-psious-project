import { getToken } from '../utils/localStorage';
export const addTask = (request) => {
    const END_POINT = 'http://localhost:3000/task/add';

    return makePostApiCall(END_POINT, request);
}

export const deleteTask = (request) => {
    const END_POINT = 'http://localhost:3000/task/delete';
    return makePostApiCall(END_POINT, request);
}
export const updateTask = (request) => {
    const END_POINT = 'http://localhost:3000/task/update';
    return makePostApiCall(END_POINT, request);
}

export const getTasks = (request) => {
    const END_POINT = 'http://localhost:3000/task/get';

    return makeGetApiCall(END_POINT, request);
}

export const clearTasks = (request) => {
    const END_POINT = 'http://localhost:3000/task/clear';
    makePostApiCall(END_POINT, request);
}

const makeGetApiCall = (endpoint, parameters) => {
    return fetch(endpoint, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + getToken()
        }
    })
        .then(results => results.json()).then(results => {
            if (results.errors) return Promise.reject(results.errors);

            return results;
        })
        .catch(err => {
            return Promise.reject(err);
        });
}
const makePostApiCall = (endpoint, parameters) => {
    return fetch(endpoint, {
        method: 'POST',
        body: JSON.stringify(parameters),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + getToken()
        }
    })
        .then(results => results.json()).then(results => {
            if (results.errors) return Promise.reject(results.errors);

            return results;
        })
        .catch(err => {
            return Promise.reject(err);
        });
}