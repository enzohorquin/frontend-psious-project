import { ADD_ITEM_SUCCESS, DELETE_ITEM_SUCCESS, SET_ITEMS_SUCCESS, CLEAR_ITEMS_SUCCESS } from '../actions';
const initialState = {
    list: []
};

export default function (state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case ADD_ITEM_SUCCESS:
            return {
                ...state,
                list: [...state.list, { ...payload }]
            }
        case SET_ITEMS_SUCCESS:
            return {
                ...state,
                list: payload
            }
        case DELETE_ITEM_SUCCESS:
            let newList = state.list.reduce((acc, item) => {
                if (item.id_task !== payload.id_task)
                    return acc.concat(item);
                else return acc;

            }, []);
            return {
                ...state,
                list: newList
            }
        case CLEAR_ITEMS_SUCCESS:
            return {
                list: []
            }
        default:
            return {
                ...state
            }

    }
}