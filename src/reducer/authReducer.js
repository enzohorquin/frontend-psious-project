import { isEmpty } from 'lodash';
import { LOGIN_SUCCESS, LOGOUT_COMPLETED } from '../actions';

const initialState = {
  isAuthenticated: false,
  user: {}
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case LOGOUT_COMPLETED: {
      return {
        isAuthenticated: false,
        user: {},
      }
    }
    default:
      return state;
  }
}
