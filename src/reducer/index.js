import { combineReducers } from 'redux';
import authReducer from './authReducer';
import toDoReducer from './toDoReducer';
import errorReducer from './errorReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    list: toDoReducer,
    errors: errorReducer,
});

export default rootReducer;