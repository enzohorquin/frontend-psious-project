import jwtDecoded from 'jwt-decode'
export const setToken = (token) => {
    localStorage.setItem('user', token);
}
export const getDecodedToken = () => {
    return jwtDecoded(localStorage.getItem('user'));
}

export const removeToken = () => {
    localStorage.removeItem('user');
}
export const getToken = () => {
    return localStorage.getItem('user');
}

export const saveList = (state) => {
    try {
        const serializedList = JSON.stringify(state.list.list);
        localStorage.setItem('list-store', serializedList);
    } catch (err) {
        console.log('Could not save list into localStorage');
    }
}

export const loadList = (state) => {
    try {
        const serializedList = localStorage.getItem('list-store');
        if (serializedList === null) return undefined;
        return JSON.parse(serializedList);

    } catch (err) {
        return undefined;
    }
}