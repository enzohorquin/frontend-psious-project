import React from 'react';

export function withAuth(WrappedComponent) {
    return class extends React.Component {

        componentDidMount() {
            const { history } = this.props;
            if (!localStorage.getItem('user') && !this.props.isLogged) {
                history.push('login');
            } else {
                if (!this.props.isLogged)
                    this.props.restoreLogin({ history });
            }
        }
        render() {
            return <WrappedComponent {...this.props} />
        }
    }
}

export default withAuth;
