import React from 'react';
import Item from '../Item';

export const List = ({ list, deleteToDo }) => {

    const listItems = list.map((item) => {
        const { id_task, description } = item;
        return (
            <Item
                key={id_task}
                id_task={id_task}
                description={description}
                deleteToDo={deleteToDo}
            />
        )
    })
    return (
        <ul className="list-group">
            {listItems}
        </ul>
    );
}


export default List;