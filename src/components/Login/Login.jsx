import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { Link } from 'react-router-dom';
import withAuth from '../withAuth';

export class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      errors: {}
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.errors;
  }

  _onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });

  };

  _onSubmit = e => {
    const { username, password } = this.state;
    const { history } = this.props;
    this.props.clearErrors();
    e.preventDefault();
    const user = {
      username,
      password,
      history,
    };

    this.props.logIn(user);
  };

  render() {
    const { errors = {} } = this.props;
    return (
      <div className="container" style={{ marginTop: "50px", width: "700px" }}>
        <h2>Login</h2>
        <form onSubmit={this._onSubmit} onChange={this._onChange}>
          <div className="form-group">
            <input
              type="text"
              placeholder="Username"
              name="username"
              className={classnames("form-control form-control-lg", {
                "is-invalid": errors.username ? errors.username : ''
              })}
            />
            {errors.username && (
              <div className="invalid-feedback">{errors.username}</div>
            )}
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Password"
              name="password"
              className={classnames("form-control form-control-lg", {
                "is-invalid": errors.password ? errors.password : ''
              })}
            />
            {errors.password && (
              <div className="invalid-feedback">{errors.password}</div>
            )}
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Login User
            </button>
            <Link to="/signup" className="btn btn-link">Don't have an account? Sign Up</Link>
          </div>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  logIn: PropTypes.func.isRequired,
  errors: PropTypes.object
};
export default withAuth(Login);
