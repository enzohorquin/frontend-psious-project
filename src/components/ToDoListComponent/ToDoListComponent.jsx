import React from 'react';
import classnames from "classnames";
import './styles.css';
import List from '../List';
import withAuth from '../withAuth';

export class ToDoListComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: ''
        }
    }

    componentDidMount() {
        const { getToDoList } = this.props;
        getToDoList();

    }

    _onClickAddToDO = (event) => {
        event.preventDefault();
        const { description } = this.state;
        const { addToDo } = this.props;
        addToDo({ description });
        event.target.value = '';
        this.setState({ description: '' })
    }
    _onChange = e => {
        e.persist();
        this.setState({
            [e.target.name]: e.target.value
        });

    };

    _onClickDeleteToDo = (id_task) => {
        const { deleteToDo } = this.props;
        deleteToDo({ id_task });
    }

    _onClickClear = () => {
        const { clearList } = this.props;
        clearList();
    }
    _onClickLogOut = () => {
        const { logOut, history } = this.props;
        logOut({ history });

    }

    render() {
        const { list, errors } = this.props;
        const itemExists = list.length > 0 ?
            <List
                list={list}
                deleteToDo={this._onClickDeleteToDo}
            />
            :
            <div>No Items Added Yet!</div>
        return (
            <div>
                <nav className="navbar navbar-light bg-light">
                    <form className="form-inline">
                        <input
                            type="text"
                            name="description"
                            placeholder="New to do item"
                            className={classnames("form-control", {
                                "is-invalid": errors.add ? errors.add : ''
                            })}
                            onChange={(event) => this._onChange(event)}
                            value={this.state.description}
                        />
                        <button type="submit" style={{ marginLeft: '10px', marginTop: '5px' }} onClick={this._onClickAddToDO} className="btn btn-outline-success my-2 my-sm-0">Add To Do</button>
                        <button type="submit" style={{ marginLeft: '10px', marginTop: '5px' }} onClick={this._onClickLogOut} className="btn btn-primary my-2 my-sm-0">Log Out</button>
                    </form>
                </nav>
                <div className="container">
                    {itemExists}
                </div>

            </div >
        )
    }

}
ToDoListComponent.defaultProps = {
    list: []
}

export default withAuth(ToDoListComponent);
