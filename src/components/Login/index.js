import Login from "./Login";
import { connect } from "react-redux";
import { LOGIN_REQUEST, CLEAR_ERRORS, RESTORE_LOGIN } from "../../actions";
import { makeActionCreator } from '../../utils/actionCreators';
const mapStateToProps = state => ({
  errors: state.errors.errors,
  isLogged: state.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  logIn: payload => dispatch(makeActionCreator(LOGIN_REQUEST, payload)),
  clearErrors: () => dispatch(makeActionCreator(CLEAR_ERRORS)),
  restoreLogin: payload => dispatch(makeActionCreator(RESTORE_LOGIN, payload)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
